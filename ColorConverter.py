# code from https://gitlab.com/luiz_hlc/color_conversion

import numpy as np
YCBCR_WEIGHTS = {
    # Spec: (K_r, K_g, K_b) with K_g = 1 - K_r - K_b
    "ITU-R_BT.709": (0.2126, 0.7152, 0.0722)
}


def rgb2ycbcr(rgb):
    """RGB to YCbCr conversion.
    Using ITU-R BT.709 coefficients.
    """
    r = rgb[:, :, 0]/255
    g = rgb[:, :, 1]/255
    b = rgb[:, :, 2]/255
    Kr, Kg, Kb = YCBCR_WEIGHTS["ITU-R_BT.709"]
    y = Kr * r + Kg * g + Kb * b
    cb = 0.5 * (b - y) / (1 - Kb) + 0.5
    cr = 0.5 * (r - y) / (1 - Kr) + 0.5
    ycbcr = np.empty(rgb.shape, dtype='int')
    ycbcr[:, :, 0] = np.round(y*255).astype(int)
    ycbcr[:, :, 1] = np.round(cb*255).astype(int)
    ycbcr[:, :, 2] = np.round(cr*255).astype(int)
    return ycbcr


def ycbcr2rgb(ycbcr):
    """YCbCr to RGB conversion.
    Using ITU-R BT.709 coefficients.
    """
    y = ycbcr[:, :, 0]/255
    cb = ycbcr[:, :, 1]/255
    cr = ycbcr[:, :, 2]/255
    Kr, Kg, Kb = YCBCR_WEIGHTS["ITU-R_BT.709"]
    r = y + (2 - 2 * Kr) * (cr - 0.5)
    b = y + (2 - 2 * Kb) * (cb - 0.5)
    g = (y - Kr * r - Kb * b) / Kg
    rgb = np.empty(ycbcr.shape, dtype='int')
    rgb[:, :, 0] = np.round(r*255).astype(int)
    rgb[:, :, 1] = np.round(g*255).astype(int)
    rgb[:, :, 2] = np.round(b*255).astype(int)
    return rgb
