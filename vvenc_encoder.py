import os


def encode_list_vvencFFapp(path_to_folder: str, video_names_list,
                           path_to_vvencFFapp: str, bitstream_dir: str, path_to_cfg: str = 'randomaccess_faster.cfg'):
    qps = [22, 27, 32, 37]
    for video in video_names_list:
        for qp_value in qps:
            video_path = os.path.join(path_to_folder, video)
            print(type(str(qp_value)))
            bitstream_path = os.path.join(bitstream_dir, video.replace('.y4m', '_') + f'qp{str(qp_value)}' + '.bin')
            vvenc_cmd = f'{path_to_vvencFFapp} --InputFile {video_path} --QP {str(qp_value)} -c {path_to_cfg} -b {bitstream_path}'
            os.system(vvenc_cmd)

encode_list_vvencFFapp('/home/gabrielafs/Documentos/UFSC/Video_coding', ['bowing_cif.y4m'], 
                        '/home/gabrielafs/codecs/vvcodec/vvenc/bin/release-static/vvencFFapp', 
                        '/home/gabrielafs/Documentos/UFSC/Video_coding')
