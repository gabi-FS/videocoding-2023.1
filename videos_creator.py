import numpy as np
import ColorConverter
from PIL import Image
import os

dataset_path = '/home/gabriela/Documents/vimeo-90k/vimeo_triplet'
# path to VIMEO-90K dataset

writing_path = '/home/gabriela/Documents/vimeo-90k/created_videos'
# path to write the created video files


def create_yuv_video(path: str, first_number: int, second_number: int, video_path: str):
    dir = os.path.join(path, '{:05d}'.format(first_number))
    dir = os.path.join(dir, '{:04d}'.format(second_number))
    im1 = Image.open(os.path.join(dir, 'im1.png'))
    im3 = Image.open(os.path.join(dir, 'im3.png'))
    data_im1 = np.array(im1)
    data_im3 = np.array(im3)
    im1_ycbcr = ColorConverter.rgb2ycbcr(data_im1)
    im3_ycbcr = ColorConverter.rgb2ycbcr(data_im3)
    # print(im1_ycbcr.shape)

    video_name = os.path.join(
        video_path, f'video-{first_number}-{second_number}.y4m')

    f = open(video_name, 'ab')
    header = f'YUV4MPEG2 W{str(im1.width)} H{str(im1.height)} F1:4\n'
    header = header.encode()

    f.write(header)
    for im in (im1_ycbcr, im3_ycbcr):
        f.write(b'FRAME\n')
        f.write(np.array(im[:, :, 0], np.uint8).tobytes())
        f.write(np.array(im[:, :, 1], np.uint8)[::2, ::2].tobytes())
        f.write(np.array(im[:, :, 2], np.uint8)[::2, ::2].tobytes())
        # print(im1_ycbcr[::2, ::2].shape)

    f.close()


'''
with open('../ice_cif.y4m', 'rb') as f:
    for i in range(2):
        a = f.readline()
        print(a)


create_yuv_video(os.path.join(dataset_path, 'target/00001'), 389)

print(len(os.listdir(os.path.join(dataset_path, 'target/00001'))))
'''

d_path = os.path.join(dataset_path, 'sequences')
w_path = os.path.join(writing_path, 'sequences')
if not os.path.exists(w_path):
    os.mkdir(w_path)

for i in range(1, 79):
    total_n = len(os.listdir(os.path.join(d_path, '{:05d}'.format(i))))
    new_dir = os.path.join(w_path, '{:05d}'.format(i))
    if not os.path.exists(new_dir):
        os.mkdir(new_dir)
    for n in range(1, total_n+1):
        video_dir = os.path.join(new_dir, '{:04d}'.format(n))
        if not os.path.exists(video_dir):
            os.mkdir(video_dir)
        create_yuv_video(d_path, i, n, video_dir)
